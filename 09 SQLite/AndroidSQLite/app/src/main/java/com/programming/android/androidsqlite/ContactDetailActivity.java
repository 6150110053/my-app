package com.programming.android.androidsqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ContactDetailActivity extends AppCompatActivity {

    Bundle extras;
    private ContactHandle handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_detail);

        extras = getIntent().getExtras();
        handler = new ContactHandle(getApplicationContext());
        ImageView iv_photo = (ImageView) findViewById(R.id.iv_contact_photo);
        iv_photo.setImageBitmap(convertToBitmap(extras.getByteArray("photograph")));
        TextView tv_name = (TextView) findViewById(R.id.tv_contact_name);
        tv_name.setText(extras.getString("name"));
        TextView tv_phone = (TextView) findViewById(R.id.tv_contact_phone);
        tv_phone.setText(extras.getString("phone"));
        TextView tv_email = (TextView) findViewById(R.id.tv_contact_email);
        tv_email.setText(extras.getString("email"));
        TextView tv_address = (TextView) findViewById(R.id.tv_contact_address);
        tv_address.setText(extras.getString("address"));
        Button btn_back = (Button) findViewById(R.id.btn_back_to_contact);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                finish();
                Intent intent = new Intent(ContactDetailActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
    private Bitmap convertToBitmap(byte[] b){
        return BitmapFactory.decodeByteArray(b, 0, b.length);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(1, 1, 0, "Edit");
        menu.add(1,2,1,"Delete");
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemid = item.getItemId();
        switch(itemid){
            case 1:
                Toast.makeText(getApplicationContext(), "Edit", Toast.LENGTH_LONG).show();
                EditContact();
                break;
            case 2:
                // Deleteing Contact
                if(handler.deleteContact(extras.getInt("id"))){
                    Toast.makeText(getApplicationContext(), "Contact has been deleted.",
                            Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(ContactDetailActivity.this, MainActivity.class);
                    startActivity(intent);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    public void EditContact(){
        Intent intent = new Intent(ContactDetailActivity.this, EditContact.class);
        intent.putExtras(extras);
        startActivity(intent);
    }


}