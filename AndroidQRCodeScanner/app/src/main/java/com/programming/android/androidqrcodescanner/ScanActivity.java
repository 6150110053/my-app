package com.programming.android.androidqrcodescanner;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScanActivity extends AppCompatActivity implements
        ZXingScannerView.ResultHandler{
    private ZXingScannerView mScannerView;
    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        mScannerView = new ZXingScannerView(this); // Programmatically initialize thescanner view
        setContentView(mScannerView); // Set the scanner view as thecontent view
    }
    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scanresults.
                mScannerView.startCamera(); // Start camera on resume
        mScannerView.setFlash(true);
        mScannerView.setAutoFocus(true);
    }
    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera(); // Stop camera on pause
    }
    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        Log.v("QRcodeScanner", rawResult.getText()); // Prints scan results
        Log.v("QRcodeScanner", rawResult.getBarcodeFormat().toString()); // Prints the scanformat (qrcode, pdf417 etc.)
        MainActivity.tvresult.setText(rawResult.getText());
        onBackPressed();
        // If you would like to resume scanning, call this method below:
        //mScannerView.resumeCameraPreview(this);
    }
}
